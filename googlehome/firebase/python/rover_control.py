import sys, json

data = json.loads(sys.stdin.readline())

response = {"result" : data}

print(json.dumps(response))
