#!/usr/bin/python3
# -*- coding: utf-8 -*-

# import 
import time
import pigpio
import threading

# GPIO setup
pi = pigpio.pi()
## Use Pin
RightPwmPin = 12
RightTruePin = 20 
RightBackPin = 21
LeftPwmPin = 6
LeftTruePin = 19
LeftBackPin = 26
## function
def gpioSetup():
    # Right Pin set
    pi.set_mode(RightPwmPin, pigpio.OUTPUT) # PWM pin set
    pi.set_mode(RightTruePin, pigpio.OUTPUT)
    pi.set_mode(RightBackPin, pigpio.OUTPUT)
    # Letf Pin set
    pi.set_mode(LeftPwmPin, pigpio.OUTPUT) # PWM pin set
    pi.set_mode(LeftTruePin, pigpio.OUTPUT)
    pi.set_mode(LeftBackPin, pigpio.OUTPUT)

# Rotation function
## RightMotor Set function
### Advance
def trueRightRotation():
    pi.write(RightTruePin, 1)
def reverseRightRotation():
    pi.write(RightBackPin, 1) 
### Stop
def stopBothRight():
    pi.write(RightTruePin,0)
    pi.write(RightBackPin,0)
def trueRightRotationStop():
    pi.write(RightTruePin, 0)
def reverseRightRotationStop():
    pi.write(RightBackPin, 0) 
## LeftMotor Set function
### Advance
def trueLeftRotation():
    pi.write(LeftTruePin, 1)
def reverseLeftRotation():
    pi.write(LeftBackPin, 1)
### Stop
def stopBothLeft():
    pi.write(LeftTruePin,0)
    pi.write(LeftBackPin,0)
def trueLeftRotationStop():
    pi.write(LeftTruePin, 0)
def reverseLeftRotationStop():
    pi.write(LeftBackPin, 0)

# Advance function
def advance():
    threading.Thread(target = trueRightRotation() ).start()
    threading.Thread(target = trueLeftRotation() ).start()
def stop():
    threading.Thread(target = stopBothRight() ).start()
    threading.Thread(target = stopBothLeft() ).start()
def rightSpinTurn():
    threading.Thread(target = trueRightRotation() ).start()
    threading.Thread(target = reverseLeftRotation() ).start()
def leftSpinTurn():
    threading.Thread(target = reverseRightRotation).start()
    threading.Thread(target = trueLeftRotation).start()
def back():
    threading.Thread(target = reverseRightRotation() ).start()
    threading.Thread(target = reverseLeftRotation() ).start()
def rightPivotTurn():
    threading.Thread(target = trueRightRotation() ).start()
    threading.Thread(target = stopBothLeft() ).start()
def leftPivotTurn():
    threading.Thread(target = stopBothRight() ).start()
    threading.Thread(target = trueLeftRotation() ).start()


'''
メモ
並列処理のやり方
thread_1 = threading.Thread(target=func1)
thread_2 = threading.Thread(target=func2)

thread_1.start()
thread_2.start()
'''
