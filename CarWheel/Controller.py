#!/usr/bin/python3
# -*- coding: utf-8 -*-

# import

import getch
import MotorControl


def main():
    MotorControl.gpioSetup()
    while True:
        if 'w' == getch.getch():
            MotorControl.advance()
        elif 's' == getch.getch():
            MotorControl.back()
        elif 'a' == getch.getch():
            MotorControl.rightSpinTurn()
        elif 'd' == getch.getch():
            MotorControl.leftSpinTurn()

if __name__ == "__main__":
    main()

